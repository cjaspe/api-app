"""Beneficiaries nodels."""

# Django
from django.db import models
from django.contrib.auth.models import User

# Custom import
from apps.users.models import Profile


class Beneficiary(models.Model):
	"""Beneficiary model."""

	user = models.ForeignKey(User, null=True, blank=False, on_delete=models.CASCADE)

	first_name = models.CharField(max_length=20, null=False, blank=False)

	last_name = models.CharField(max_length=20, null=False, blank=False)

	dni_ci = models.CharField(max_length=20, null=False, blank=False)

	email = models.EmailField(max_length=20, unique=True, null=False, blank=False)

	phone_number = models.CharField(max_length=20, blank=True)

	address = models.TextField(blank=True)
	
	def __str__(self):
		"""Return First Name."""
		return self.first_name