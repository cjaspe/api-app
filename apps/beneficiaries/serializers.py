"""Beneficiaries Serializer."""

from rest_framework import serializers
from .models import Beneficiary

class BeneficiarySerializer(serializers.HyperlinkedModelSerializer):
	"""Beneficiaries serializer."""
	class Meta:
		model = Beneficiary
		fields = (
            'user',
            'first_name',
            'last_name',
            'dni_ci',
            'email',
            'phone_number',
            'address',
            'url'
        )