"""Beneficiaries Views resources."""

# Django.
from django.shortcuts import render

# DRF.
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

# Custom.
from .models import Beneficiary
from .serializers import BeneficiarySerializer

class BeneficiaryViewSet(viewsets.ModelViewSet):
	queryset = Beneficiary.objects.all()
	serializer_class = BeneficiarySerializer
	
	# Permission.
	permission_class = IsAuthenticated

	# Queryset filter user.
	def get_queryset(self):
		return self.queryset.filter(user=self.request.user)

	# Capture user.
	def perform_create(self, serializer):
		serializer.save(user=self.request.user)
