# Generated by Django 2.1 on 2018-08-29 15:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('beneficiaries', '0004_auto_20180827_1236'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='beneficiary',
            name='profile',
        ),
        migrations.AddField(
            model_name='beneficiary',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
