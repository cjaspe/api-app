"""Beneficiaries admin classes."""

# Django
from django.contrib import admin

# Models
from apps.beneficiaries.models import Beneficiary


@admin.register(Beneficiary)
class BeneficiaryAdmin(admin.ModelAdmin):
	"""Profile admin."""
	pass