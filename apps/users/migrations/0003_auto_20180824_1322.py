# Generated by Django 2.1 on 2018-08-24 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20180824_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='address',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='dni_ci',
            field=models.CharField(blank=True, max_length=20),
        ),
    ]
