"""User Views resources."""

# Django.
from django.shortcuts import render
from django.contrib.auth.models import User

# DRF.
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

# Custom.
from .models import Profile
from .serializers import ProfileSerializer
from .serializers import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer
	# Permission
	permission_class = IsAuthenticated


class ProfileViewSet(viewsets.ModelViewSet):
	queryset = Profile.objects.all()
	serializer_class = ProfileSerializer
	# Permission
	permission_class = IsAuthenticated