"""User models."""

#Django
from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
	"""Profile model.

	Proxy model that extends the base data 
	with other information"""


	user = models.OneToOneField(User, on_delete=models.CASCADE)

	dni_ci = models.CharField(max_length=20, blank=False)

	phone_number = models.CharField(max_length=20, blank=True)

	address = models.TextField(blank=True)
	
	picture = models.ImageField(
		upload_to='apps/users/picture', 
		blank=True, 
		null=True
	)


	def __str__(self):
		"""Return username."""
		return self.user.username


