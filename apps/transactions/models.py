"""Beneficiaries nodels."""

#Django
from django.db import models
from django.contrib.auth.models import User

#Custom import
from apps.users.models import Profile
from apps.beneficiaries.models import Beneficiary



class Transaction(models.Model):
	""" transaction users model."""

	user = models.ForeignKey(User, null=True, blank=False, on_delete=models.CASCADE)

	beneficiary = models.ForeignKey(Beneficiary, null=False, blank=False, on_delete=models.CASCADE)

	quantity = models.DecimalField(max_digits=20, decimal_places=5, null=False, blank=False)

	date = models.DateTimeField(auto_now_add=True)

	#Status transaction
	PROCESS = 'process'
	CANCELLED = 'cancelled'
	REJECTED = 'rejected'
	APPROVED = 'approved'
	STATUS_CHOICES = (
		(PROCESS, 'process'),
		(CANCELLED, 'cancelled'),
		(REJECTED, 'rejected'),
		(APPROVED, 'approved'),
	)
	status = models.CharField(
		max_length = 20,
		choices = STATUS_CHOICES,
		default = PROCESS,
	)

	def __str__(self):
		"""Return Quantity."""
		return self.quantity