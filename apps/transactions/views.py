"""Transactions Views resources."""

# Django.
from django.shortcuts import render

# DRF.
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

# Custom.
from .models import Transaction
from .serializers import TransactionSerializer

class TransactionViewSet(viewsets.ModelViewSet):
	queryset = Transaction.objects.all()
	serializer_class = TransactionSerializer
	
	# Permission.
	permission_class = IsAuthenticated

	# Queryset filter user.
	def get_queryset(self):
		return self.queryset.filter(user=self.request.user)

	# Capture user.
	def perform_create(self, serializer):
		serializer.save(user=self.request.user)