"""Transactions admin classes."""

# Django
from django.contrib import admin

# Models
from apps.transactions.models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
	"""Profile admin."""
	pass