"""Transactions Serializer."""

from rest_framework import serializers
from .models import Transaction

class TransactionSerializer(serializers.HyperlinkedModelSerializer):
	"""Transactions serializer."""
	class Meta:
		model = Transaction
		fields = (
            'user',
            'beneficiary',
            'quantity',
            'date',
            'status',
            'url'
        )