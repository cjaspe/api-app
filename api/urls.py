"""URL Configuration"""

# Django
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

# Custom
# Restframework routers
from rest_framework import routers

from apps.users.views import UserViewSet
from apps.users.views import ProfileViewSet
from apps.transactions.views import TransactionViewSet
from apps.beneficiaries.views import BeneficiaryViewSet


router = routers.DefaultRouter()

router.register('users', UserViewSet)
router.register('profiles', ProfileViewSet)
router.register('transactions', TransactionViewSet)
router.register('beneficiaries', BeneficiaryViewSet)

urlpatterns = [
    
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
	#path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	path('api/', include('rest_auth.urls')),
    path('api/registration/', include('rest_auth.registration.urls'))
]


